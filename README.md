# Some curious circuits
## [circlotron](amplifiers/circlotron_simplified)
Simple transistor variant of [Circlotron](https://en.wikipedia.org/wiki/Circlotron) finded on internet
![](pics/circ_sim4.jpg)
![](pics/sim4.jpg)
![](pics/circ.jpg)

Traced on kicad and simulated with ngspice

## [Accord 201 mod from class B to class A (JLH) concept ](amplifiers/akkord201_mod_class_B2A_concept)

[concept](pics/Akkord_201_mod_to_jlh_concept.jpg)

[Comparing simulations of original scheme and concept](https://tube.tchncs.de/w/1RjVUTcoGMyEUj5ePk4p4q)

Real scheme:

![](pics/akkord_last_mod_scheme.png)

![](pics/akkord_last_mod_graphics.png)

The circuit was replicated in Kicad 7 and modeled using ngspice.

Be aware that Class A amplifiers run at full power even it did not amplify anything, so good cooling and power feeder are required.

![](pics/akkord_mod.jpg)

## [variants of JLH amplifier](amplifiers/jlh)

[JLH-69](amplifiers/jlh/jlh-69)

[JLH-like 2005 scheme](amplifiers/jlh/jlh_like_2005)

![](pics/example.jpg)

[JLH69 variant](amplifiers/jlh/archive/JLH69_lp)

![](pics/JLH69_lp_smd.gif)

Traced on kicad and simulated with ngspice
